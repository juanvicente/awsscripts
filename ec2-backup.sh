#!/bin/bash
DATE=`/bin/date '+%Y%m%d%H%M%S'`
ec2-describe-instances --region=eu-west-1 --filter "tag:backup=true"  |grep  "backup"| sort|uniq|awk -v OFS='\t' '{print $3}' > /tmp/backup.txt

for obj0 in $(cat /tmp/backup.txt)
do
name="$(ec2-describe-tags --region=eu-west-1 --filter "resource-id=$obj0"| grep Name|awk -v OFS='\t' '{print $5}')"
description="$name-backup-$DATE"
ec2-create-image --no-reboot --region=eu-west-1 $obj0 --name "$description" --description "$description"
done

for obj0 in $(cat /tmp/backup.txt)
do
#remove the older images for keep the last 5 images
name="$(ec2-describe-tags --region=eu-west-1 --filter "resource-id=$obj0"| grep Name|awk -v OFS='\t' '{print $5}')"
ec2-describe-images --region=eu-west-1 | sort -r|grep $name |awk -v OFS='\t' '{print $2}' > /tmp/backup_delete.txt
backups="$(cat /tmp/backup_delete.txt|wc -l)"
echo "$obj0 $backups"
	if [ "$backups" -gt 5 ]; then
	flag=$(echo $((backups-5)))
	echo $flag
	grep -m $flag $obj0  /tmp/backup_delete.txt > /tmp/backup_delete_old.txt
	for obj1 in $(cat /tmp/backup_delete_old.txt)
	do
	ec2-deregister --region=eu-west-1 $obj1
	done
	fi
done
