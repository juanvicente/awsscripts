#!/usr/bin/env python
# author: Juan Vicente Herrera 
# juanvicenteherrera.eu
# requisites: boto python module installed
# sudo pip install boto
# script that start, stop or list the instances of an environment
# All of the instances of an environment has to have a tag called "Env" with one of the values defined in the config.py file
# Example of usage ./ec2-instances.py stop Dev
# Dev is the value of Tag "Env" in all of the instances that compose dev environment and is defined in config.py
# as the value of Env Tag for dev instances
# ./ec2-instances.py list|start|stop Prod|Stage|Dev|All
# By default is not allowed to stop production env and you can avoid that some instances be stopped or started by the script
# setting its instance id in "exclusions" array included in config.py


import boto.ec2
from boto.ec2 import EC2Connection
import sys
import re
import config
import argparse
from optparse import OptionParser, make_option


csv_file = open('instances.csv','w+')

def main():
	
	print ("%s"%(str(sys.argv[1])))
	if (len(sys.argv) < 4):
		parser = argparse.ArgumentParser()
		parser.add_argument("Prod|Stage|Dev|All|Temp|Nat|QaBooks|DevBooks", help="environment of the instances")
		parser.add_argument("list|start|stop", help="action that you want to execute in all of the servers of an environment")
		parser.add_argument("eu-west-1|ap-southeast-1", help="region of your instances")
		args = parser.parse_args()
	else:
		print "entrando"
		print ("%s"%(str(sys.argv)))
		if (str(sys.argv[1]) == "stop" and str(sys.argv[2]) == "Prod"):
			print "Invalid option. Not allow to shutdown Production environment without authorization"
			print "list|start|stop are the only valid values"
		elif (str(sys.argv[1]) == "stop" and str(sys.argv[2]) == "Nat"):
                        print "Invalid option. Not allow to shutdown nat instances without authorization"
                        print "list|start|stop are the only valid values"
        	elif (str(sys.argv[1]) == "stop" and str(sys.argv[2]) == "All"):
            		print "Invalid option. Not allow to stop the whole platform without authorization"
            		print "list|start|stop are the only valid values"
        	elif (str(sys.argv[1]) == "start" and str(sys.argv[2]) == "All"):
            		print "Invalid option. Not allow to start the whole platform without authorization"
            		print "list|start|stop are the only valid values"
        	elif (str(sys.argv[1]) != "list" and str(sys.argv[1]) != "start" and str(sys.argv[1]) != "stop"):
			print "Invalid option."
			print "list|start|stop are the only valid values"
        	elif (str(sys.argv[2]) != "Prod" and str(sys.argv[2]) != "Dev" and str(sys.argv[2]) != "DevBooks" and str(sys.argv[2]) != "QaBooks" and str(sys.argv[2]) != "Stage" and str(sys.argv[2]) != "All" and str(sys.argv[2]) != "Temp" and str(sys.argv[2]) != "Nat"):
			print "Invalid option."
			print "Prod|Stage|Dev|All|Temp|QaBooks|DevBooks|Nat are the only valid values"
		elif (str(sys.argv[3]) != "ap-southeast-1" and str(sys.argv[3]) != "eu-west-1"):
                        print "Invalid option."
                        print "ap-southeast-1|eu-west-1 are the only valid values"
		else:
			regions = boto.ec2.regions()
			for region in regions:
				if region.name == str(sys.argv[3]):
					conn_eu = region.connect(aws_access_key_id=config.aws_access_key_id,aws_secret_access_key=config.aws_secret_access_key)
    					process_instance_list(conn_eu)
    					csv_file.close()

def process_instance_list(conn_eu):
    map(build_instance_list,conn_eu.get_all_instances())
 
def build_instance_list(reservation):
    map(write_instances,reservation.instances)
 
def write_instances(instance):
    regexes_es=config.exclusions
    if (str(sys.argv[2])=="Prod"):
    	regexes = config.TagProd

    	
    elif (str(sys.argv[2])=="Stage"):
    	regexes = config.TagStage

    		
    elif (str(sys.argv[2])=="Dev"):
		regexes = config.TagDev
    elif (str(sys.argv[2])=="All"):
    	regexes = config.TagAll
    elif (str(sys.argv[2])=="Temp"):
        regexes = config.TagTemp
    elif (str(sys.argv[2])=="QaBooks"):
        regexes = config.TagQaBooks
    elif (str(sys.argv[2])=="DevBooks"):
        regexes = config.TagDevBooks
    elif (str(sys.argv[2])=="Nat"):
        regexes = config.TagNat
    if (instance.tags.get('Env') is not None):
    	if instance.tags.get('Env')  in regexes:
            csv_file.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n"%(instance.tags.get('Name'),instance.private_ip_address,instance.key_name,instance.id,instance.instance_type,instance.public_dns_name,instance.state,instance.placement,instance.architecture, instance.platform))
            print ("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n"%(instance.tags.get('Name'),instance.private_ip_address,instance.key_name,instance.id,instance.instance_type,instance.public_dns_name, instance.state,instance.placement,instance.architecture, instance.platform))
            if instance.id in regexes_es:
                print "Excluido"
            else:  
                print "en marcha"  
                if (str(sys.argv[1])=="start"):
                    instance.start()
                    print "starting"
                elif (str(sys.argv[1])=="stop"):
                    print instance.id
                    instance.stop()      
                    print "stopping"
    	                                      
    csv_file.flush()
        
if __name__=="__main__":
	 main()	
	

