#!/usr/bin/env python
# author: juanvicente.herrear@bqraeders.com
# juanvicenteherrera.eu
# requisites: boto python module installed
# sudo pip install boto
# description: deattach and attach instances of the desired elbs
import boto
import config
from boto import regioninfo
from boto import ec2

elb_region = boto.regioninfo.RegionInfo(
    name='eu-west-1', 
    endpoint='elasticloadbalancing.eu-west-1.amazonaws.com')

elb_connection = boto.connect_elb(
    aws_access_key_id=config.aws_access_key_id, 
    aws_secret_access_key=config.aws_secret_access_key, 
    region=elb_region)

ec2_region = ec2.get_region(
    aws_access_key_id=config.aws_access_key_id, 
    aws_secret_access_key=config.aws_secret_access_key, 
    region_name='eu-west-1')

ec2_connection = boto.ec2.connection.EC2Connection(
    aws_access_key_id=config.aws_access_key_id, 
    aws_secret_access_key=config.aws_secret_access_key, 
    region=ec2_region)

load_balancer = elb_connection.get_all_load_balancers(load_balancer_names=['staging2-books','staging2-stg2book-41G2U3Z3ULLD','staging2-stg2elka-1QQRQ8NLC5EZQ','staging2-stg2nubi-1UJ448N19FF7','staging2-stg2mund-1C8VCE2BDQ950','staging2-stg2fnac-12F7A5FNVX01N','staging2-stg2tr3s-XM2U55L1XB7R','staging-books','stagingBo-ElasticL-1D6ABW6PJM2XU','stagingBo-ElasticL-1D6ABW6PJM2XU','staging-k-elbkiton-RSYZ4CD7GWT7','staging-k-elbkiton-X6DDX0IRGJ08'])
for elb in load_balancer:
    instance_ids = [ instance.id for instance in elb.instances ]
    print "Instances %s dettached of Load Balancer %s" %(instance_ids,elb.name)
    elb.deregister_instances(instance_ids)
    print "Instances %s attached to Load Balancer %s" %(instance_ids,elb.name)
    elb.register_instances(instance_ids)
